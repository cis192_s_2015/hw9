""" Homework 9
-- Due Sunday, Mar. 29th at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""
from scipy import misc
import os


def separate(in_img):
    '''Take in as input a numpy (height x width x 3) array where the
    (i,j)^th element is a length 3 array of the form [r, g, b] where
    each color is a number from 0-255

    output a tuple (r_copy, g_copy, b_copy) where each element of the tuple
    is a numpy (height x width) array which is a copy of the data in the
    original array for the specific color
    '''
    pass


def make_rgb(r, g, b):
    ''' Take as input three numpy (height x width) arrays representing
    the three color channels (r, g, b) of an image and output a numpy
    (height x width x 3) array.

    If any of the inputs is None, treat it as a numpy array of
    all zeros (height x width) broadcasting an np.zeros(1) might be useful

    Assume that at least one input is not None and that any non-None inputs
    will be the same shape
    '''
    pass


def gaussian_blur(in_channel, rad, sig):
    '''
    Take as input a single color channel of an image,
    a numpy (height x width) array, and the radius rad and sigma sig which are
    paramaters of the gaussian blur algorithm.

    Perform a two-dimensional gaussian blur in a single pass over the channel
    http://en.wikipedia.org/wiki/Gaussian_blur

    Specifically:
    - use the formula on that wiki page with x^2+y^2 in the exponent
    - For each pixel (row, col) compute a gausian weight for
      the pixels in the rectangle ([row-rad, row+rad], [col-rad, col+rad])
      make sure to ignore indecies that are out of bounds like (-1, width)
    - Compute a weighted average of the pixels:


    example for (row, col) = 1, 1 and radius = 1, sigma = 0.5
    gaus_x_y = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 0), ..., (1, 1)]
    pixels = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), ..., (2, 2)]
    gaussians = [0.01167, 0.0862, 0.01167, 0.0862, 0.6366, ..., 0.01167]
    total gaus = 1.0279
    weights = [0.01134, 0.08381, 0.01134, 0.08381, 0.61934, ..., 0.01134]
    val = in_channel[0, 0] * weights[0] + in_channel[0, 1] * weights[1] + ...
    '''
    def gaus(x, y, cache={}):
        '''helper to compute the two dimensional gaussian weight
        '''
        key = (x, y)
        if key in cache:
            return cache[key]
        sig_sq = sig**2
        c = 1 / (2 * math.pi * sig_sq)
        power = -((x**2 + y**2)/(2 * sig_sq))
        res = c * math.exp(power)
        cache[key] = res
        return res

    def weighted_avg(weights, cells):
        ''' helper to perform a weighted average
        '''
        tot_weight = sum(weights)
        normed = [w / tot_weight for w in weights]
        weight_val_pairs = zip(normed, [in_channel[r, c] for r, c in cells])
        return sum([w * v for w, v in weight_val_pairs])

    pass


def sharpen(chan, threshold, amount, rad, sig):
    ''' Take as input a single color channel of an image,
    a numpy (height x width) array, a threshold pixel difference and
    a percentage in decimal form which is the amount to sharpen,
    rad and sig as parameters for the blur

    Sharpen the image by adding a blurred image
     (Each side of the edge becomes more like its surrounding side
      which emphasizes the difference between the two sides)
    - Create a gaussian blur of the image using rad and sig
    - Compute the difference in rbg values between the blur and original
    - For each pixel where the absolute value of the difference
      exceeds threshold:
          subtract the difference, scaled by amount, from the original
    '''
    pass


def svd_compress(chan, percent, base_f_name):
    '''
    The singular value decomposition (SVD) of a matrix factors a matrix into
    an set of vectors ordered by how import each vector is to the matrx
    http://en.wikipedia.org/wiki/Singular_value_decomposition
    By keeping only the most important vectors, the matrix can retain the
    most important info and throw away the rest (lossy compression)
    Similar to JPEG image compression.
    http://en.wikipedia.org/wiki/JPEG#JPEG_compression

    Take as input a single color channel of an image,
    a numpy (height x width) array,
    the percent of singular values to keep
    a file_name for pickling the raw svd and reduced svd
    and comparing the sizes

    Use scipy.linalg to compute the svd of the channel U, S, Vh
    U -> (height x height)
    S -> 1-D min(height, width) of singluar values sorted from hight to low
    Vh -> (width x width)

    If there are n singular values keep
        (k = min(round(percent*n) + 1, n)) of them
    Uk = first k columns of U
    Vhk = first k rows of Vh
    Sk = first k singular values
    Dk = (height x width) diagonal matrix out of the k singular values
       np.diag() and np.concatenate((a, b), axis=0/1) could be helpful
    the compressed image will be the matrix product Uk*Dk*Vhk

    pickle the tuple (u, s, vh) to a file base_f_name_full.pickle
    pickle the tuple of compressed values (Uk, Sk, Vhk) to a file
    base_f_name_compressed.pickle

    Look at the file size to see how much the image was compressed
     (The file size of the resulting channel is not impressive since
      the matrix product results in a height x width matrix
      just like the original.
      We would need need to think of a clever file format like JPEG
      to make use of the compression, by storing and reading the smaller
      matrices)

    return the compressed channel
    '''
    pass


def safe_create_dir(dir_name):
    ''' Provided for testing
    '''
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    if not os.path.isdir(dir_name):
        raise Exception('"{}" exists but is not a directory'.format(dir_name))


def to_rgb(img, f_name):
    ''' Provided for testing
    '''
    dest = 'rgb'
    safe_create_dir(dest)
    names = ['{}_{}'.format(pre, f_name) for pre in ['red', 'green', 'blue']]
    r_path, g_path, b_path = [os.path.join(dest, x) for x in names]
    sep_r, sep_g, sep_b = separate(img)
    r = make_rgb(sep_r, None, None)
    misc.imsave(r_path, r)
    g = make_rgb(None, sep_g, None)
    misc.imsave(g_path, g)
    b = make_rgb(None, None, sep_b)
    misc.imsave(b_path, b)


def to_sharpen(img, f_name):
    ''' Provided for testing
    '''
    thres, amount, rad, sig = 0, 0.05, 2, 1
    dest = 'sharpen'
    safe_create_dir(dest)
    r, g, b = separate(img)
    s_r, s_g, s_b = [sharpen(c, thres, amount, rad, sig)
                     for c in (r, g, b)]
    sharp = make_rgb(s_r, s_g, s_b)
    misc.imsave(os.path.join(dest, 'sharp_{}'.format(f_name)), sharp)


def to_svd(img, f_name):
    ''' Provided for testing
    '''
    dest = 'svd'
    safe_create_dir(dest)
    r, g, b = separate(img)
    args = zip([r, g, b], ['red', 'green', 'blue'])
    s_r, s_g, s_b = [svd_compress(c, 0.1, n) for c, n in args]
    svd_comp = make_rgb(s_r, s_g, s_b)
    misc.imsave(os.path.join(dest, 'svd_{}'.format(f_name)), svd_comp)


def to_blur(img, f_name):
    ''' Provided for testing
    '''
    rad, sig = 3, 1.25
    dest = 'blur'
    safe_create_dir(dest)
    r, g, b = separate(img)
    b_r, b_g, b_b = [gaussian_blur(c, rad, sig) for c in (r, g, b)]
    blur = make_rgb(b_r, b_g, b_b)
    misc.imsave(os.path.join(dest, 'blur_{}'.format(f_name)), blur)


def main():
    img_dir = 'imgs'
    img_file = 'monalisa.png'
    path = os.path.join(img_dir, img_file)
    img = misc.imread(path)  # you need to install Pillow (pip install Pillow)

    # uncomment this when you implement separate and make_rgb
    #to_rgb(img, img_file)

    # uncomment this when you implement gaussian_blur
    #to_blur(img, img_file)


    # uncomment this when you implement sharpen
    #to_sharpen(img, img_file)

    # uncomment this when you implement svd_compress
    #to_svd(img, img_file)


if __name__ == "__main__":
    main()
