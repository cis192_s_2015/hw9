import unittest
import os
import pickle
from scipy import misc
import numpy
from hw9 import *


def array_eq(a, b, printer=False):
    err = 5
    sub = a - b
    high = 255 - sub
    diff = numpy.minimum(sub, high)
    if printer:
        print(diff, numpy.amax(diff))
    return (diff < err).all()


def write_and_read(img):
    name = os.path.join('junk_file_for_write_and_read.png')
    misc.imsave(name, img)
    return misc.imread(name)


class TestHomework9(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
        self.img_dir = 'imgs'
        img_file = 'monalisa.png'
        self.test_dir = 'test'
        self.img = misc.imread(os.path.join(self.img_dir, img_file))

    # separate
    def test_separate(self):
        with open(os.path.join(self.test_dir, 'separate.pickle'), 'rb') as pik:
            ans_r, ans_g, ans_b = pickle.load(pik)
        r, g, b = separate(self.img)
        self.assertTrue(array_eq(r, ans_r))
        self.assertTrue(array_eq(g, ans_g))
        self.assertTrue(array_eq(b, ans_b))

    # make rgb
    def test_make_rgb(self):
        with open(os.path.join(self.test_dir, 'separate.pickle'), 'rb') as pik:
            r, g, b = pickle.load(pik)
        red_only = make_rgb(r, None, None)
        red_only = write_and_read(red_only)
        ans = misc.imread(os.path.join(self.test_dir, 'red_monalisa.png'))
        self.assertTrue(array_eq(red_only, ans))
        full = make_rgb(r, g, b)
        full = write_and_read(full)
        ans = misc.imread(os.path.join(self.test_dir, 're_made_monalisa.png'))
        self.assertTrue(array_eq(full, ans))

    # gaussian blur
    def test_gaussian_blur(self):
        blur = my_to_blur(self.img, 2, 1)
        blur = write_and_read(blur)
        ans = misc.imread(os.path.join(self.test_dir, 'blur_monalisa.png'))
        self.assertTrue(array_eq(blur, ans))
        blur2 = my_to_blur(self.img, 3, 1.25)
        blur2 = write_and_read(blur2)
        ans = misc.imread(os.path.join(self.test_dir, 'blur2_monalisa.png'))
        self.assertTrue(array_eq(blur2, ans))

    # sharpen
#    def test_sharpen(self):
#        sharpen = my_to_sharpen(self.img, 0, 0.05)
#        sharpen = write_and_read(sharpen)
#        ans = misc.imread(os.path.join(self.test_dir, 'sharp_monalisa.png'))
#        self.assertTrue(array_eq(sharpen, ans))
#        sharpen2 = my_to_sharpen(self.img, 250, 0.2)
#        sharpen2 = write_and_read(sharpen2)
#        ans = misc.imread(os.path.join(self.test_dir, 'sharp2_monalisa.png'))
#        self.assertTrue(array_eq(sharpen2, ans))

    # svd compress
    def test_svd_compress(self):
        svd = my_to_svd(self.img)
        svd = write_and_read(svd)
        ans = misc.imread(os.path.join(self.test_dir, 'svd_monalisa.png'))
        self.assertTrue(array_eq(svd, ans))
        for f_name in ['blue_compressed.pickle', 'blue_full.pickle']:
            with open(os.path.join(self.test_dir, f_name), 'rb') as pik:
                ans_u, ans_s, ans_v = pickle.load(pik)
            with open(f_name, 'rb') as pik:
                u, s, v = pickle.load(pik)
            self.assertTrue(array_eq(ans_u, u))
            self.assertTrue(array_eq(ans_s, s))
            self.assertTrue(array_eq(ans_v, v))


def my_to_sharpen(img, thres, amount):
    ''' Provided for testing
    '''
    rad, sig = 2, 1
    r, g, b = separate(img)
    s_r, s_g, s_b = [sharpen(c, thres, amount, rad, sig)
                     for c in (r, g, b)]
    return make_rgb(s_r, s_g, s_b)


def my_to_svd(img):
    ''' Provided for testing
    '''
    r, g, b = separate(img)
    args = zip([r, g, b], ['red', 'green', 'blue'])
    s_r, s_g, s_b = [svd_compress(c, 0.1, n) for c, n in args]
    return make_rgb(s_r, s_g, s_b)


def my_to_blur(img, rad, sig):
    ''' Provided for testing
    '''
    r, g, b = separate(img)
    b_r, b_g, b_b = [gaussian_blur(c, rad, sig) for c in (r, g, b)]
    return make_rgb(b_r, b_g, b_b)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
